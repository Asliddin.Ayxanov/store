<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Asliddin
Route::post('/login', 'LoginController@authenticate');

Route::prefix('/products')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/', 'ProductController@GetProducts');
        Route::get('/get_category', 'ProductController@GetCategory');
        Route::post('/create_product', 'ProductController@CreateProducts');
        Route::post('/upload', 'ProductController@upload');
        Route::post('/update_product', 'ProductController@UpdateProducts');
        Route::post('/multidelete', 'ProductController@Deletemulti');
        Route::post('/delete_product', 'ProductController@DeleteProducts');
    });

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
        return User::All();
    });
Route::post('/uploads', 'ProductController@uploads');

Route::prefix('/statistic')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/', 'PiosController@GetPios');
        Route::get('/get', 'PiosController@Get');
        Route::get('/getstore', 'PiosController@GetStore');
        Route::get('/category/{id}', 'PiosController@GetCategory');
        Route::post('/create_product', 'PiosController@CreateProducts');
        Route::post('/upload', 'PiosController@upload');
        Route::post('/update_product', 'PiosController@UpdateProducts');
    });

//end Asliddin


//Sherozjon
Route::get('/logout', 'UsersController@Logout');
Route::prefix('/user')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/auth_user', 'UsersController@AuthUser');
        Route::get('/get_users', 'UsersController@GetUsers');
        Route::post('/create_user', 'UsersController@CreateUser');
        Route::post('/update_user', 'UsersController@UpdateUser');
        Route::post('/delete_user', 'UsersController@DeleteUser');
        Route::post('/more_delete_user', 'UsersController@MoreDeleteUser');
    });

Route::prefix('/role')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_roles', 'RolesController@GetRoles');
        Route::post('/create_role', 'RolesController@CreateRole');
        Route::post('/update_role', 'RolesController@UpdateRole');
        Route::post('/delete_role', 'RolesController@DeleteRole');
    });
//end Sherozjon


//Axror
Route::prefix('/category')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_category', 'CategoriesController@GetCategory');
        Route::get('/store', 'CategoriesController@store');
        Route::post('/create_category', 'CategoriesController@CreateCategory');
        Route::post('/delete_category', 'CategoriesController@DeleteCategory');
        Route::post('/update_category', 'CategoriesController@UpdateCategory');
    });

Route::prefix('/suppliers')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_suppliers', 'SuppliersController@GetSuppliers');
        Route::get('/store', 'SuppliersController@store');
        Route::post('/create_suppliers', 'SuppliersController@CreateSuppliers');
        Route::post('/delete_suppliers', 'SuppliersController@DeleteSuppliers');
        Route::post('/update_suppliers', 'SuppliersController@UpdateSuppliers');
    });
//end Axror



//O`tkir
Route::prefix('/store')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_store', 'StoreController@index');
        Route::get('/getUsers', 'StoreController@getUser');
        Route::post('/create_store', 'StoreController@store');
        Route::put('/update_store', 'StoreController@update');
        Route::post('/deletes_stores', 'StoreController@multiDelete');
        Route::delete('/delete_store/{id}', 'StoreController@destroy');
    });

    Route::prefix('/dastavchik')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_store', 'StoreController@index');
        Route::post('/create_store', 'StoreController@store');
        Route::put('/update_store', 'StoreController@update');
        Route::post('/deletes_stores', 'StoreController@multiDelete');
        Route::delete('/delete_store/{id}', 'StoreController@destroy');
    });
//end O`tkir