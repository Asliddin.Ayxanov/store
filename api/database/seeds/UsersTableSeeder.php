<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = '[{"name":"Dashboard","title":"Dashboard","url":"/","iconUrl":"inbox","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Store","title":"Store","url":"/store","iconUrl":"inbox","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Category","title":"Category","url":"/category","iconUrl":"category","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Product","title":"Product","url":"/product","iconUrl":"shopping_cart","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Statistic","title":"Statistic","url":"/statistic","iconUrl":"leaderboard","read":true,"write":null,"update":null,"delete":null,"input":true,"output":true},{"name":"Users","title":"Users","url":"/users","iconUrl":"person","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Roles","title":"Roles","url":"/roles","iconUrl":"settings","read":true,"write":true,"update":true,"delete":true,"input":null,"output":null},{"name":"Suppliers","title":"Suppliers","url":"/supplier","iconUrl":"home","read":true,"write":false,"update":false,"delete":true,"input":null,"output":null}]';
        DB::table("roles")->insert(
            [
                "name" => "Super Admin",
                "permission" => $permission,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("users")->insert(
            [
                "name" => "ItProgress",
                "email" => "ItProgress@gmail.com",
                "password" => bcrypt("12345678"),
                "role_id" => "1",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
    }
}
