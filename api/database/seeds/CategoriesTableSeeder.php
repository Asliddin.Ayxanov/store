<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("categories")->insert(
            [
                "name" => "Go`shtli Mahsulotlar",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("categories")->insert(
            [
                "name" => "Sutli Mahsulotlar",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("categories")->insert(
            [
                "name" => "Qandolat",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("categories")->insert(
            [
                "name" => "Ichimliklar",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
        DB::table("categories")->insert(
            [
                "name" => "Konstovar",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],    
        );
    }
}
