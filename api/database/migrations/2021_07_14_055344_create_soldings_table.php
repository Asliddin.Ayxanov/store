<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soldings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pios_id');
            $table->float('amount');
            $table->float('price');
            $table->timestamps();

            $table
                ->foreign('pios_id')
                ->references('id')
                ->on('pios')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soldings');
    }
}
