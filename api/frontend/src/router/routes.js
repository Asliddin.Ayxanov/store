const routes = [
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('pages/Index.vue'),
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('pages/users/users.vue'),
      },
      {
        path: '/roles',
        name: 'Roles',
        component: () => import('pages/roles/roles.vue'),
      },
      {
        path: '/store',
        name: 'Stores',
        component: () => import('pages/store/store.vue'),
      },
      {
        path: '/category',
        name: 'Category',
        component: () => import('pages/category/category.vue'),
      },
      {
        path: '/statistic',
        name: 'Statistic',
        component: () => import('pages/statistics/statistic.vue'),
      },
      {
        path: '/product',
        name: 'Product',
        component: () => import('pages/products/products.vue'),
      },
      {
        path: '/supplier',
        name: 'Supplier',
        component: () => import('pages/suppliers/supplier.vue'),
      },
    ],
  },
  {
    path: '/logout',
    component: () => import('layouts/LoginLayout.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes