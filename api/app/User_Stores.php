<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Stores extends Model
{

    protected $table = 'user_stores';

    protected $fillable = ['user_id', 'stores_id'];

}
