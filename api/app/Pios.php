<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pios extends Model
{
    protected $fillable = ['id', 'product_id', 'count', 'price', 'payme'];

    public function soldings()
    {
        return $this->hasMany('App\Soldings');
    }

    public function products()
    {
        return $this->belongsTo('App\Products');
    }
}
