<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\Stores;
use App\Http\Resources\SupplierCollection;

class SuppliersController extends Controller
{
    public function GetSuppliers(Request $request) 
    {
        $suppliers = Supplier::orderBy('id')->get();
        return SupplierCollection::collection($suppliers);
    }
    public function store()
    {
        return Stores::orderBy('id', 'DESC')->get();
    }
    public function CreateSuppliers(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $data = [
            'name' => $request->name,
            'stores_id' => $request->store_id,
        ];

        $suppliers = Supplier::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'type' => $request->type,
            'stores_id' => $request->store_id,
        ]);
        return $suppliers;
    }
    public function UpdateSuppliers(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $data = [
            'name' => $request->name,
            'phone' => $request->phone,
            'type' => $request->type,
            'stores_id' => $request->store_id,
        ];
        //bazaga shunaqa nom bilan save qilingan yani stores_id bilan
        $suppliers = Supplier::where('id', $request->id)->update($data);
        return $suppliers;
    }
    public function DeleteSuppliers(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        return Supplier::where('id', $request->id)->delete();

    }
}
