<?php

namespace App\Http\Controllers;
use App\Stores;
use App\User;
use App\Products;
use App\Categories;
use Illuminate\Http\Request;

class PiosController extends Controller
{
    public function Get(Request $request)
    {
        return Products::orderBy('id', 'Desc')->get();
    }
    public function GetStore(Request $request)
    {
        return $request->user()->store;
    }
    public function GetCategory($id)
    {
        if (empty($id)) {
            return;
        }
        return Categories::where('stores_id', $id)
            ->orderBy('id', 'Desc')
            ->get();
    }
}
