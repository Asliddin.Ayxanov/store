<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stores;
use App\User;
use App\User_Stores;
use Illuminate\Support\Carbon;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $stor=Stores::orderBy('id','DESC')->get();
       
       $store = User::where('id',auth()->user()->id)
       ->with('store.user')
       ->get();
        return response()->json(["store"=>$store]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $data=$request->validate([
            'name'=>'required|unique:App\Stores,name',
            'adress'=>'required',
        ]);
       $store = Stores::create($data);

     
    if($request->user_id != []){
            foreach($request->user_id as $use_id){
                User_Stores::create([
                    'user_id'=>$use_id,
                    'stores_id'=>$store->id
                ]);
            }
        }
        return $store;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data=$request->validate([
            'name'=>'required',
            'adress'=>'required'
        ]);
        if($request->user_id == []){
            return response()->json([
                "error"=>"Select user id field is required !"
            ],403);
        }
      $edit = Stores::where('id',$request->id)->first(); 
      $res = $edit->update($data); 
      User_Stores::where('stores_id',$request->id)->delete();
       foreach($request->user_id as $use_id){
            User_Stores::create([
                'user_id'=>$use_id,
                'stores_id'=>$request->id
            ]);
    }
      return $res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Stores::where('id',$id)->delete();
    }

    public function multiDelete(Request $request){
        $data = json_decode($request->data);
        foreach($data as $val){
            Stores::where('id',$val->id)->delete();
        }
        return response()->json([
            "msg"=>"Success",
            'success'=>true
        ]);
    }

    public function getUser(){
        $users = User::all();
        return response()->json(["users"=>$users,"auth"=>["name"=>auth()->user()->name,"id"=>auth()->user()->id]]);
    }
}
