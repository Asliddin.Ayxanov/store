<?php

namespace App\Http\Controllers;
use App\Categories;
use App\Stores;
use App\Http\Resources\CategoryCollection;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function GetCategory(Request $request)
    {
        $categories = Categories::orderBy('id')->get();
        return CategoryCollection::collection($categories);
    }
    public function store()
    {
        return Stores::orderBy('id', 'DESC')->get();
    }
    public function CreateCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);
        $data = [
            'name' => $request->name,
            'stores_id' => $request->store_id,
        ];

        $category = Categories::create([
            'name' => $request->name,
            'stores_id' => $request->store_id,
        ]);
        return $category;
    }
    public function DeleteCategory(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Categories::where('id', $request->id)->delete();
    }

    public function UpdateCategory(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $data = [
            'name' => $request->name,
            'stores_id' => $request->store_id,
        ];
        //bazaga shunaqa nom bilan save qilingan yani stores_id bilan
        $category = Categories::where('id', $request->id)->update($data);
        return $category;
    }
}
