<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categories;
use App\Products;
use App\Http\Resources\ProductCollection;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class ProductController extends Controller
{
    public function GetProducts(Request $request)
    {
        $dt = Products::orderBy('id', 'Desc')->get();
        return ProductCollection::collection($dt);
    }
    public function Get(Request $request)
    {
        return Products::orderBy('id', 'Desc')->get();
    }
    public function upload(Request $request)
    {
        return $request;
    }
    public function uploads(Request $request)
    {
        $request->file->storeAs(
            'uploads',
            uniqid('img_') . $request->file->getClientOriginalName()
        );
    }
    public function GetCategory()
    {
        return Categories::orderBy('id')->get();
    }
    public function CreateProducts(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'img' => 'required',
            'category_id' => 'required',
            'describe' => 'required|min:3',
        ]);

        return Products::create([
            'name' => $request->name,
            'img' => $request->img,
            'categories_id' => $request->category_id,
            'describe' => $request->describe,
        ]);
    }

    public function UpdateProducts(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:3',
            'img' => 'required',
            'category_id' => 'required',
            'describe' => 'required|min:3',
        ]);

        $data = [
            'name' => $request->name,
            'img' => $request->img,
            'categories_id' => $request->category_id,
            'describe' => $request->describe,
        ];
        return Products::where('id', $request->id)->update($data);
    }

    public function DeleteProducts(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);
        return Products::where('id', $request->id)->delete();
    }
    public function Deletemulti(Request $request)
    {
        $data = json_decode($request->data);
        foreach ($data as $val) {
            Products::where('id', $val->id)->delete();
        }
        return response()->json([
            'msg' => 'Success',
            'success' => true,
        ]);
    }
}
