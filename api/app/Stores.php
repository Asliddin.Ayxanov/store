<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    protected $fillable = ['id', 'name', 'adress'];

    public function user()
    {
        return $this->belongsToMany('App\User','user_stores');
    }

    public function category()
    {
        return $this->hasMany('App\Categories');
    }
}
