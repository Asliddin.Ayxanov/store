<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['id', 'name', 'img', 'describe', 'categories_id'];

    public function pios()
    {
        return $this->hasMany('App\Pios');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories');
    }
}
