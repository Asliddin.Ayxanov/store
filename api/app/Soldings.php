<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soldings extends Model
{
    protected $fillable = ["id","pios_id","amount","price"];

    public function pios(){
        return $this->belongsTo("App\Pios");
    }
}
