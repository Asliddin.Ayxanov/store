<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['id', 'name', 'stores_id'];

    public function products()
    {
        return $this->hasMany('App\Products');
    }
    public function stores()
    {
        return $this->belongsTo('App\Stores');
    }
}
