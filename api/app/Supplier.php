<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = ["id", "name","phone","type", "stores_id"];

    public function stores(){
        return $this->belongsTo("App\Stores");
    }
}
